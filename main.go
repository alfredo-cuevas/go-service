package main

import (
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/alfredo-cuevas/go-service/internal/controller"
	"bitbucket.org/alfredo-cuevas/go-service/internal/service"
	"bitbucket.org/alfredo-cuevas/go-service/internal/store"
	"bitbucket.org/alfredo-cuevas/go-service/internal/usecase"

	"github.com/sirupsen/logrus"

	_ "github.com/golang/mock/mockgen/model"
)

const (
	// AppName application name
	AppName = "go-service-demo"
)

func main() {
	logger := logrus.New()

	// Services, Store init
	// Service layer involves all the dependencies that the service
	// needs to perform its operations
	someService := service.NewSomeService("param", "value")
	store := store.NewPostgres()

	// UseCase init
	// UseCase layer involves all the business logic of the application,
	// the usecase are split by responsabilities. Eg something, status
	orderUC := usecase.NewOrder(store, someService)
	somethingUC := usecase.NewSomething(someService)
	statusUC := usecase.NewStatus(AppName)

	// Controller init
	// Controller layer involves all the http stuff...
	orderC := controller.NewOrder(orderUC)
	somethingC := controller.NewSomething(somethingUC)
	statusC := controller.NewStatus(statusUC)

	// Create router
	// The router expects multiple controllers with its specific handlers
	r := controller.NewRouter(orderC, somethingC, statusC)

	// Define stop signal for the end of excecution
	stop := make(chan os.Signal, 1)
	signal.Notify(
		stop,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGHUP,
	)

	go func() {
		address := ":8080"
		if err := r.Start(address); err != nil {
			logger.Fatal("something went wrong")
		}
	}()

	<-stop

	logger.Info("shutting down server...")
}
