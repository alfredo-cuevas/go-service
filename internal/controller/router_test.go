package controller

import (
	"reflect"
	"testing"

	"github.com/labstack/echo"
)

func TestNewRouter(t *testing.T) {
	type args struct {
		order     OrderController
		something SomethingController
		status    StatusController
	}
	tests := []struct {
		name string
		args args
		want *echo.Echo
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewRouter(tt.args.order, tt.args.something, tt.args.status); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewRouter() = %v, want %v", got, tt.want)
			}
		})
	}
}
