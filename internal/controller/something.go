package controller

import (
	"net/http"

	"github.com/labstack/echo"

	"bitbucket.org/alfredo-cuevas/go-service/internal/entity"
)

//go:generate mockgen -destination=./mocks/mock_something.go -package=mocks bitbucket.org/alfredo-cuevas/go-service/internal/controller SomethingUseCase

// SomethingUseCase -
type SomethingUseCase interface {
	DoSomething(info string) (map[string]int, error)
}

// Something -
type Something struct {
	UseCase SomethingUseCase
}

// NewSomething -
func NewSomething(uc SomethingUseCase) *Something {
	return &Something{
		UseCase: uc,
	}
}

// SomethingHandler -
func (h *Something) SomethingHandler(c echo.Context) error {
	var data entity.Request
	if err := c.Bind(&data); err != nil {
		return c.String(http.StatusBadRequest, "invalid json")
	}

	if data.Info == "" {
		return c.String(http.StatusBadRequest, "invalid info")
	}

	resp, err := h.UseCase.DoSomething(data.Info)
	if err != nil {
		return c.String(http.StatusBadRequest, "something went wrong")
	}

	return c.JSON(http.StatusOK, resp)
}
