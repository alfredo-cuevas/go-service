package controller

import (
	"reflect"
	"testing"

	"github.com/labstack/echo"
)

func TestNewOrder(t *testing.T) {
	type args struct {
		uc OrderUseCase
	}
	tests := []struct {
		name string
		args args
		want *Order
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOrder(tt.args.uc); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOrder() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrder_GetHandler(t *testing.T) {
	type fields struct {
		UseCase OrderUseCase
	}
	type args struct {
		c echo.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Order{
				UseCase: tt.fields.UseCase,
			}
			if err := h.GetHandler(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Order.GetHandler() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrder_CreateHandler(t *testing.T) {
	type fields struct {
		UseCase OrderUseCase
	}
	type args struct {
		c echo.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Order{
				UseCase: tt.fields.UseCase,
			}
			if err := h.CreateHandler(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Order.CreateHandler() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrder_UpdateHandler(t *testing.T) {
	type fields struct {
		UseCase OrderUseCase
	}
	type args struct {
		c echo.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Order{
				UseCase: tt.fields.UseCase,
			}
			if err := h.UpdateHandler(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Order.UpdateHandler() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrder_DeleteHandler(t *testing.T) {
	type fields struct {
		UseCase OrderUseCase
	}
	type args struct {
		c echo.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Order{
				UseCase: tt.fields.UseCase,
			}
			if err := h.DeleteHandler(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Order.DeleteHandler() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
