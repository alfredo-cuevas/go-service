package controller

import (
	"github.com/labstack/echo"
)

// StatusController -
type StatusController interface {
	StatuszHandler(c echo.Context) error
	HealthzHandler(c echo.Context) error
}

// OrderController -
type OrderController interface {
	GetHandler(c echo.Context) error
	CreateHandler(c echo.Context) error
	UpdateHandler(c echo.Context) error
	DeleteHandler(c echo.Context) error
}

// SomethingController -
type SomethingController interface {
	SomethingHandler(c echo.Context) error
}

// New -
func NewRouter(order OrderController, something SomethingController,
	status StatusController) *echo.Echo {

	e := echo.New()

	e.GET("/statusz", status.StatuszHandler)
	e.GET("/healthz", status.HealthzHandler)

	e.GET("/order/:id", order.GetHandler)
	e.POST("/order", order.CreateHandler)
	e.PUT("/order/:id", order.UpdateHandler)
	e.DELETE("/order/:id", order.DeleteHandler)

	e.POST("/something", something.SomethingHandler)

	return e
}
