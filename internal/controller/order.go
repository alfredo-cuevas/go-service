package controller

import (
	"context"
	"net/http"

	"github.com/labstack/echo"

	"bitbucket.org/alfredo-cuevas/go-service/internal/entity"
)

//go:generate mockgen -destination=./mocks/mock_order.go -package=mocks bitbucket.org/alfredo-cuevas/go-service/internal/controller OrderUseCase

// OrderUseCase -
type OrderUseCase interface {
	CreateOrder(ctx context.Context, order entity.OrderRequest) (entity.OrderResponse, error)
	GetOrderByID(ctx context.Context, id string) (entity.OrderResponse, error)
	UpdateOrderByID(ctx context.Context, id string, order entity.OrderRequest) (entity.OrderResponse, error)
	DeleteOrderByID(ctx context.Context, id string) error
}

// Order -
type Order struct {
	UseCase OrderUseCase
}

// NewOrder -
func NewOrder(uc OrderUseCase) *Order {
	return &Order{
		UseCase: uc,
	}
}

// GetHandler -
func (h *Order) GetHandler(c echo.Context) error {
	ctx := c.Request().Context()

	id := c.Param("id")
	resp, err := h.UseCase.GetOrderByID(ctx, id)
	if err != nil {
		return c.String(http.StatusBadRequest, "something went wrong")
	}

	return c.JSON(http.StatusOK, resp)
}

// CreateHandler -
func (h *Order) CreateHandler(c echo.Context) error {
	ctx := c.Request().Context()

	var data entity.OrderRequest
	if err := c.Bind(&data); err != nil {
		return c.String(http.StatusBadRequest, "invalid json")
	}

	resp, err := h.UseCase.CreateOrder(ctx, data)
	if err != nil {
		return c.String(http.StatusBadRequest, "something went wrong")
	}

	return c.JSON(http.StatusOK, resp)
}

// UpdateHandler -
func (h *Order) UpdateHandler(c echo.Context) error {
	ctx := c.Request().Context()

	id := c.Param("id")
	var data entity.OrderRequest
	if err := c.Bind(&data); err != nil {
		return c.String(http.StatusBadRequest, "invalid json")
	}

	resp, err := h.UseCase.UpdateOrderByID(ctx, id, data)
	if err != nil {
		return c.String(http.StatusBadRequest, "something went wrong")
	}

	return c.JSON(http.StatusOK, resp)
}

// DeleteHandler -
func (h *Order) DeleteHandler(c echo.Context) error {
	ctx := c.Request().Context()

	id := c.Param("id")
	err := h.UseCase.DeleteOrderByID(ctx, id)
	if err != nil {
		return c.String(http.StatusBadRequest, "something went wrong")
	}

	return c.NoContent(http.StatusOK)
}
