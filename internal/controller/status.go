package controller

import (
	"net/http"

	"github.com/labstack/echo"
)

//go:generate mockgen --build_flags=--mod=mod -destination=./mocks/mock_status.go -package=mocks bitbucket.org/alfredo-cuevas/go-service/internal/controller StatusUseCase

// StatusUseCase -
type StatusUseCase interface {
	Statusz() (string, error)
	Healthz() (string, error)
}

// Status -
type Status struct {
	UseCase StatusUseCase
}

// NewStatus -
func NewStatus(uc StatusUseCase) *Status {
	return &Status{
		UseCase: uc,
	}
}

// StatuszHandler -
func (h *Status) StatuszHandler(c echo.Context) error {
	resp, err := h.UseCase.Statusz()
	if err != nil {
		return c.String(http.StatusBadRequest, "something went wrong")
	}

	return c.JSON(http.StatusOK, resp)
}

// HealthzHandler -
func (h *Status) HealthzHandler(c echo.Context) error {
	resp, err := h.UseCase.Healthz()
	if err != nil {
		return c.String(http.StatusBadRequest, "something went wrong")
	}

	return c.JSON(http.StatusOK, resp)
}
