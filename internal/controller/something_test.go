package controller

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"bitbucket.org/alfredo-cuevas/go-service/internal/controller/mocks"

	"github.com/golang/mock/gomock"
	"github.com/labstack/echo"
)

func TestNewSomething(t *testing.T) {
	type args struct {
		uc SomethingUseCase
	}

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	SomethingC := &Something{
		UseCase: mocks.NewMockSomethingUseCase(mockCtrl),
	}
	tests := []struct {
		name string
		args args
		want *Something
	}{
		{
			name: "Success NewSomething",
			args: args{
				uc: SomethingC.UseCase,
			},
			want: SomethingC,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewSomething(tt.args.uc); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewSomething() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSomething_HandlerSomething(t *testing.T) {
	type fields struct {
		mockUseCaseConfigurer func(*mocks.MockSomethingUseCase)
	}
	type args struct {
		body string
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantResp string
		wantCode int
		wantErr  bool
	}{
		{
			name: "Success HandlerSomething",
			fields: fields{
				mockUseCaseConfigurer: func(m *mocks.MockSomethingUseCase) {
					m.EXPECT().
						DoSomething("test").
						Return(map[string]int{"test": 1}, nil)
				},
			},
			args: args{
				body: `{"info":"test"}`,
			},
			wantResp: "{\"test\":1}\n",
			wantCode: http.StatusOK,
			wantErr:  false,
		},
	}
	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {
			e := echo.New()
			req := httptest.NewRequest(http.MethodPost, "/something", bytes.NewReader([]byte(tt.args.body)))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)

			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mUC := mocks.NewMockSomethingUseCase(mockCtrl)
			if tt.fields.mockUseCaseConfigurer != nil {
				tt.fields.mockUseCaseConfigurer(mUC)
			}

			h := &Something{
				UseCase: mUC,
			}

			if err := h.SomethingHandler(c); (err != nil) != tt.wantErr {
				t.Errorf("Something.HandlerSomething() error = %v, wantErr %v", err, tt.wantErr)
			}

			respBody := rec.Body
			if respBody.String() != tt.wantResp {
				t.Errorf("Something.HandlerSomething() expected = %v, got %v", tt.wantResp, respBody.String())
			}
			if rec.Code != tt.wantCode {
				t.Errorf("Something.HandlerSomething() expected = %v, got %v", tt.wantCode, rec.Code)
			}
		})
	}
}
