package controller

import (
	"reflect"
	"testing"

	"github.com/labstack/echo"
)

func TestNewStatus(t *testing.T) {
	type args struct {
		uc StatusUseCase
	}
	tests := []struct {
		name string
		args args
		want *Status
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewStatus(tt.args.uc); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStatus_StatuszHandler(t *testing.T) {
	type fields struct {
		UseCase StatusUseCase
	}
	type args struct {
		c echo.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Status{
				UseCase: tt.fields.UseCase,
			}
			if err := h.StatuszHandler(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Status.StatuszHandler() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStatus_HealthzHandler(t *testing.T) {
	type fields struct {
		UseCase StatusUseCase
	}
	type args struct {
		c echo.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Status{
				UseCase: tt.fields.UseCase,
			}
			if err := h.HealthzHandler(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Status.HealthzHandler() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
