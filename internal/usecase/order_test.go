package usecase

import (
	"context"
	"reflect"
	"testing"

	"bitbucket.org/alfredo-cuevas/go-service/internal/entity"
	"bitbucket.org/alfredo-cuevas/go-service/internal/usecase/mocks"
	"github.com/golang/mock/gomock"
)

func TestNewOrder(t *testing.T) {
	type args struct {
		store       Store
		someService OrderSomeService
	}
	tests := []struct {
		name string
		args args
		want Order
	}{
		{
			name: "Success NewOrder",
			args: args{
				store:       &mocks.MockStore{},
				someService: &mocks.MockOrderSomeService{},
			},
			want: Order{
				Store:       &mocks.MockStore{},
				SomeService: &mocks.MockOrderSomeService{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOrder(tt.args.store, tt.args.someService); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOrder() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrder_GetOrderByID(t *testing.T) {
	type fields struct {
		mockStoreConfigurer       func(*mocks.MockStore)
		mockSomeServiceConfigurer func(*mocks.MockOrderSomeService)
	}
	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.OrderResponse
		wantErr bool
	}{
		{
			name: "Success GetOrderByID",
			fields: fields{
				mockStoreConfigurer: func(m *mocks.MockStore) {
					m.EXPECT().
						GetOrderByID(gomock.Any(), "someID").
						Return(entity.OrderResponse{
							ID:        "someID",
							UserID:    "someUserID",
							CreatedAt: "createdAt",
						}, nil)
				},
				mockSomeServiceConfigurer: func(m *mocks.MockOrderSomeService) {},
			},
			args: args{
				ctx: context.Background(),
				id:  "someID",
			},
			want: entity.OrderResponse{
				ID:        "someID",
				UserID:    "someUserID",
				CreatedAt: "createdAt",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mStore := mocks.NewMockStore(mockCtrl)
			if tt.fields.mockStoreConfigurer != nil {
				tt.fields.mockStoreConfigurer(mStore)
			}

			mSomeService := mocks.NewMockOrderSomeService(mockCtrl)
			if tt.fields.mockSomeServiceConfigurer != nil {
				tt.fields.mockSomeServiceConfigurer(mSomeService)
			}

			uc := Order{
				Store:       mStore,
				SomeService: mSomeService,
			}
			got, err := uc.GetOrderByID(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Order.GetOrderByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Order.GetOrderByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrder_CreateOrder(t *testing.T) {
	type fields struct {
		mockStoreConfigurer       func(*mocks.MockStore)
		mockSomeServiceConfigurer func(*mocks.MockOrderSomeService)
	}
	type args struct {
		ctx   context.Context
		order entity.OrderRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.OrderResponse
		wantErr bool
	}{
		{
			name: "Success CreateOrder",
			fields: fields{
				mockStoreConfigurer: func(m *mocks.MockStore) {
					m.EXPECT().
						CreateOrder(gomock.Any(), entity.OrderRequest{UserID: "someUserIDValue"}).
						Return(entity.OrderResponse{
							ID:        "someID",
							UserID:    "someUserIDValue",
							CreatedAt: "createdAt",
						}, nil)
				},
				mockSomeServiceConfigurer: func(m *mocks.MockOrderSomeService) {
					m.EXPECT().
						GetValue().
						Return("Value")
				},
			},
			args: args{
				ctx: context.Background(),
				order: entity.OrderRequest{
					UserID: "someUserID",
				},
			},
			want: entity.OrderResponse{
				ID:        "someID",
				UserID:    "someUserIDValue",
				CreatedAt: "createdAt",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mStore := mocks.NewMockStore(mockCtrl)
			if tt.fields.mockStoreConfigurer != nil {
				tt.fields.mockStoreConfigurer(mStore)
			}

			mSomeService := mocks.NewMockOrderSomeService(mockCtrl)
			if tt.fields.mockSomeServiceConfigurer != nil {
				tt.fields.mockSomeServiceConfigurer(mSomeService)
			}

			uc := Order{
				Store:       mStore,
				SomeService: mSomeService,
			}
			got, err := uc.CreateOrder(tt.args.ctx, tt.args.order)
			if (err != nil) != tt.wantErr {
				t.Errorf("Order.CreateOrder() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Order.CreateOrder() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrder_UpdateOrderByID(t *testing.T) {
	type fields struct {
		Store       Store
		SomeService OrderSomeService
	}
	type args struct {
		ctx   context.Context
		id    string
		order entity.OrderRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.OrderResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := Order{
				Store:       tt.fields.Store,
				SomeService: tt.fields.SomeService,
			}
			got, err := uc.UpdateOrderByID(tt.args.ctx, tt.args.id, tt.args.order)
			if (err != nil) != tt.wantErr {
				t.Errorf("Order.UpdateOrderByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Order.UpdateOrderByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrder_DeleteOrderByID(t *testing.T) {
	type fields struct {
		Store       Store
		SomeService OrderSomeService
	}
	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := Order{
				Store:       tt.fields.Store,
				SomeService: tt.fields.SomeService,
			}
			if err := uc.DeleteOrderByID(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("Order.DeleteOrderByID() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
