package usecase

import (
	"context"
	"fmt"

	"bitbucket.org/alfredo-cuevas/go-service/internal/entity"
)

//go:generate mockgen -destination=./mocks/mock_order_some_service.go -package=mocks bitbucket.org/alfredo-cuevas/go-service/internal/usecase OrderSomeService
//go:generate mockgen -destination=./mocks/mock_store.go -package=mocks bitbucket.org/alfredo-cuevas/go-service/internal/usecase Store

// OrderSomeService -
type OrderSomeService interface {
	GetValue() string
}

// Store -
type Store interface {
	CreateOrder(ctx context.Context, order entity.OrderRequest) (entity.OrderResponse, error)
	GetOrderByID(ctx context.Context, id string) (entity.OrderResponse, error)
	UpdateOrderByID(ctx context.Context, id string, order entity.OrderRequest) (entity.OrderResponse, error)
	DeleteOrderByID(ctx context.Context, id string) error
}

// Order -
type Order struct {
	Store       Store
	SomeService OrderSomeService
}

// NewOrder -
func NewOrder(store Store, someService OrderSomeService) Order {
	return Order{
		Store:       store,
		SomeService: someService,
	}
}

// GetOrderByID -
func (uc Order) GetOrderByID(ctx context.Context, id string) (entity.OrderResponse, error) {
	// validate anything
	// business logic stuff
	resp, err := uc.Store.GetOrderByID(ctx, id)
	if err != nil {
		return entity.OrderResponse{}, err
	}

	return resp, nil
}

// CreateOrder -
func (uc Order) CreateOrder(ctx context.Context, order entity.OrderRequest) (entity.OrderResponse, error) {
	// validate anything
	if order.UserID == "" {
		return entity.OrderResponse{}, fmt.Errorf("invalid userID")
	}

	// also do some request to external services...
	// business logic
	val := uc.SomeService.GetValue()
	order.UserID += val

	resp, err := uc.Store.CreateOrder(ctx, order)
	if err != nil {
		return entity.OrderResponse{}, err
	}

	return resp, nil
}

// UpdateOrderByID -
func (uc Order) UpdateOrderByID(ctx context.Context, id string, order entity.OrderRequest) (entity.OrderResponse, error) {
	// validate anything
	if order.UserID == "" {
		return entity.OrderResponse{}, fmt.Errorf("invalid userID")
	}

	resp, err := uc.Store.UpdateOrderByID(ctx, id, order)
	if err != nil {
		return entity.OrderResponse{}, err
	}

	return resp, nil
}

// DeleteOrderByID -
func (uc Order) DeleteOrderByID(ctx context.Context, id string) error {
	// Do some validations or anything before delete
	err := uc.Store.DeleteOrderByID(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
