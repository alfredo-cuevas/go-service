package store

import (
	"context"
	"reflect"
	"testing"

	"bitbucket.org/alfredo-cuevas/go-service/internal/entity"
)

func TestNewPostgres(t *testing.T) {
	tests := []struct {
		name string
		want Postgres
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewPostgres(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewPostgres() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_GetOrderByID(t *testing.T) {
	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name    string
		s       Postgres
		args    args
		want    entity.OrderResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Postgres{}
			got, err := s.GetOrderByID(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Postgres.GetOrderByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Postgres.GetOrderByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_CreateOrder(t *testing.T) {
	type args struct {
		ctx   context.Context
		order entity.OrderRequest
	}
	tests := []struct {
		name    string
		s       Postgres
		args    args
		want    entity.OrderResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Postgres{}
			got, err := s.CreateOrder(tt.args.ctx, tt.args.order)
			if (err != nil) != tt.wantErr {
				t.Errorf("Postgres.CreateOrder() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Postgres.CreateOrder() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_UpdateOrderByID(t *testing.T) {
	type args struct {
		ctx   context.Context
		id    string
		order entity.OrderRequest
	}
	tests := []struct {
		name    string
		s       Postgres
		args    args
		want    entity.OrderResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Postgres{}
			got, err := s.UpdateOrderByID(tt.args.ctx, tt.args.id, tt.args.order)
			if (err != nil) != tt.wantErr {
				t.Errorf("Postgres.UpdateOrderByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Postgres.UpdateOrderByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_DeleteOrderByID(t *testing.T) {
	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name    string
		s       Postgres
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Postgres{}
			if err := s.DeleteOrderByID(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("Postgres.DeleteOrderByID() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
