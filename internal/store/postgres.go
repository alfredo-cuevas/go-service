package store

import (
	"context"

	"github.com/google/uuid"

	"bitbucket.org/alfredo-cuevas/go-service/internal/entity"
)

// Postgres -
type Postgres struct {
}

// NewPostgres -
func NewPostgres() Postgres {
	return Postgres{}
}

// GetOrderByID -
func (s Postgres) GetOrderByID(ctx context.Context, id string) (entity.OrderResponse, error) {
	resp := entity.OrderResponse{
		ID:        id,
		UserID:    "9a59699c-a89c-4ef5-8e6c-9acac14f73d9",
		CreatedAt: "Some date",
	}

	return resp, nil
}

// CreateOrder -
func (s Postgres) CreateOrder(ctx context.Context, order entity.OrderRequest) (entity.OrderResponse, error) {
	createdAt := "something generated on store side"

	resp := entity.OrderResponse{
		ID:        uuid.New().String(),
		UserID:    order.UserID,
		CreatedAt: createdAt,
	}

	return resp, nil
}

// UpdateOrder -
func (s Postgres) UpdateOrderByID(ctx context.Context, id string, order entity.OrderRequest) (entity.OrderResponse, error) {
	resp := entity.OrderResponse{
		ID:        id,
		UserID:    order.UserID,
		CreatedAt: "some stored value",
	}

	return resp, nil
}

// DeleteOrder -
func (s Postgres) DeleteOrderByID(ctx context.Context, id string) error {
	// Just return nil, as successfully deleted

	return nil
}
