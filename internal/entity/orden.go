package entity

// OrderRequest -
type OrderRequest struct {
	UserID string `json:"user_id"`
}

// OrderResponse -
type OrderResponse struct {
	ID        string `json:"id"`
	UserID    string `json:"user_id"`
	CreatedAt string `json:"create_at"`
}
