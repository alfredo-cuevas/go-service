package service

// SomeService -
type SomeService struct {
	Param string
	Value string
}

// NewSomeService -
func NewSomeService(param, value string) *SomeService {
	return &SomeService{
		Param: param,
		Value: value,
	}
}

// GetParam -
func (s *SomeService) GetParam() string {
	return s.Param
}

// GetValue -
func (s *SomeService) GetValue() string {
	return s.Value
}
