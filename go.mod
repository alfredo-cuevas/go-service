module bitbucket.org/alfredo-cuevas/go-service

go 1.17

require (
	github.com/golang/mock v1.6.0
	github.com/google/uuid v1.3.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/sirupsen/logrus v1.8.1
)

require (
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.0.1 // indirect
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/text v0.3.3 // indirect
)
